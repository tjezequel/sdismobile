angular.module('starter.services', ['ionic.utils'])

.service('LoginService', function ($q, $http, $window) {
    return {
        loginUser: function (name, pw) {
            var deferred = $q.defer();
            var promise = deferred.promise;

            $http({
                method: 'POST',
                url: 'https://api.gaspard.xyz/auth/login',
                data: {"auth_user":name, "auth_password":pw}
            }).then(function (response) {
                if(response.data['code'] == 100){
                    deferred.resolve('Welcome '+ name + '!');
                    $window.localStorage['matricule'] = response.data['matricule'];
                } else {
                    deferred.reject("Wrong Credentials");
                }
            });
            promise.success = function (fn) {
              promise.then(fn);
              return promise;
            }
            promise.error = function (fn) {
              promise.then(null, fn);
              return promise;
            }
            return promise;
        }
    }
})

.service('dayService', function(){
    var Servday;

    var setDay = function(day){
        Servday = day;
    }

    var getDay = function(){
      return Servday;
    }

    return {
      setDay: setDay,
      getDay: getDay
    };
})

.service('dispoService', function ($q, $http, $window) {
      

});
