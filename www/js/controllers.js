angular.module('starter.controllers', ['ionic.utils'])

.controller('loginCtrl', function ($scope, LoginService, $ionicPopup, $state) {
    $scope.data = {};

    $scope.login = function(){
        LoginService.loginUser($scope.data.username, $scope.data.password).success(function (data) {
            $state.go('tab-dash');
        }).error(function (data) {
            var alertPopup = $ionicPopup.alert({
                title: 'Connexion Echouée',
                template: 'Vérifiez vos identifiants'
            })
        })
    }
})

.controller('dashCtrl', function ($scope, $http, $window, $state) {

    $http({
        method: 'GET',
        url: 'https://api.gaspard.xyz/fireman/'+$window.localStorage['matricule']
    }).then(function (response) {
        $scope.pompier = response.data[0];
    });

    $scope.gotoList = function () {
        $state.go('day-list');
    };

})

.controller('daysCtrl', function($scope, $http, $window, $state, dayService){

    $http({
      method: 'GET',
      url: 'https://api.gaspard.xyz/week'
    }).then(function (response) {
      $scope.days = response.data;
    });

    $scope.enterDisp = function(day){
      console.log(day);
        dayService.setDay(day);
        $state.go('day', day)
    }

})

.controller('dayCtrl', function($scope, $http, $state, dayService, $window, $ionicHistory){
    $scope.hours = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23];
    $scope.disponibilite = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    $scope.day = dayService.getDay();
  console.log($scope.day);

    $scope.rowPressed = function(index){
      console.log('Row pressed : '+index);
      $scope.disponibilite[index] += 1;
      console.log($scope.disponibilite[index]);
    };

    $scope.goBack = function() {
      $ionicHistory.goBack();
    };

    $scope.sendData = function () {
      console.log($scope.day);

      $http({
        method: 'POST',
        url: 'https://api.gaspard.xyz/availability',
        data: {pompier: $window.localStorage["matricule"], date: $scope.day.Y+"-"+$scope.day.m+"-"+$scope.day.d, dispos: $scope.disponibilite}
      }).then(function(response) {
        $ionicHistory.goBack();
      })




    }

})

.directive('iconSwitcher', function() {

  return {
    restrict : 'A',

    link : function(scope, elem, attrs) {

      var currentState = 0;

      elem.on('click', function() {

        if(currentState == 0) {
          console.log('Dispo');
          angular.element(elem).removeClass(attrs.dispo);
          angular.element(elem).addClass(attrs.dispoHors);
          currentState = 1;
        } else if (currentState == 1){
          console.log('Dispo Hors Delais');
          angular.element(elem).removeClass(attrs.dispoHors);
          angular.element(elem).addClass(attrs.indispo);
          currentState = 2;
        } else if (currentState == 2){
          console.log('Indispo');
          angular.element(elem).removeClass(attrs.indispo);
          angular.element(elem).addClass(attrs.dispo);
          currentState = 0;
        }

      });


    }
  };
});
